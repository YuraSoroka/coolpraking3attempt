﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; set; }
        public DateTime DateTimeOfTransaction { get; private set; }
        public string VehicleId{ get; private set; }

        public TransactionInfo(DateTime dateTimeOfTransaction, string vehicleId, decimal sum)
        {
            DateTimeOfTransaction = dateTimeOfTransaction;
            VehicleId = vehicleId;
            Sum = sum;
        }

        public override string ToString()
        {
            string transactionString;

            transactionString = DateTimeOfTransaction.ToLongDateString() + "  " + DateTimeOfTransaction.ToLongTimeString() + "   " + "Id of car: " + VehicleId +
                "   Written-off funds: " + Sum.ToString() + "\n";

            return transactionString;
        }
    }
}