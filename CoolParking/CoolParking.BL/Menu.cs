﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
 Интерфейс должен быть удобным и понятным пользователям, в виде консольного меню. 
Должен быть пользовательский ввод c валидацией и форматируемый вывод текста и данных.

Функциональные требования, которые должны быть доступны пользователю при работе с программой:

Вывести на экран текущий баланс Парковки.
Вывести на экран сумму заработанных денег за текущий период (до записи в лог).
Вывести на экран количество свободных/занятых мест на парковке.
Вывести на экран все Транзакции Парковки за текущий период (до записи в лог).
Вывести историю транзакций (считав данные из файла Transactions.log).
Вывести на экран список Тр. средств находящихся на Паркинге.
Поставить Тр. средство на Паркинг.
Забрать транспортное средство с Паркинга.
Пополнить баланс конкретного Тр. средства.
 */
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.Reflection;
using System.IO;

namespace CoolParking.BL
{
    class Menu
    {
        //Parking parking = Parking.getInstance();
        public const string _logFilePath = @".\Transactions.log";


        ParkingService parkingService = new ParkingService(new TimerService() , new TimerService() , new LogService(_logFilePath) ); // для реалізації логіки в меню
        public void ShowMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("***   Welcome to the CoolParking automatic system :)   ***");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(new string('-' , 58));
            Console.WriteLine("******   You can choose available function below:   ******");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\n1. Текущий баланс Парковки : ");
            Console.WriteLine("2. Вывести на экран сумму заработанных денег за текущий период (до записи в лог) ");
            Console.WriteLine("3. Количество свободных/занятых мест на парковке. ");
            Console.WriteLine("4. Все транзакции Парковки за текущий период (до записи в лог).");
            Console.WriteLine("5. История транзакций");
            Console.WriteLine("6. Список транспортных средств находящихся на паркинге. ");
            Console.WriteLine("7. Припарковать ");
            Console.WriteLine("8. Забрать транспортное средство с паркинга. ");
            Console.WriteLine("9. Пополнить баланс транспортного средства ");
            Console.WriteLine("0. Выход ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public  void MainMenu()
        {
            ConsoleKeyInfo key;
            
            do
            {
                Console.Clear();
                ShowMenu();
                key = Console.ReadKey();
                if (!Char.IsDigit(key.KeyChar))
                {
                    Console.WriteLine("\nНеобходимо нажимать только цифры!");
                    Console.ReadKey();                
                }
                else
                {
                    Console.Clear();
                    switch (key.KeyChar)
                    {
                        case '1':
                            parkingService.GetBalance();
                            break;
                        case '2':
                            parkingService.GetSumForTheLastMinute();
                            break;
                        case '3':
                            parkingService.GetFreePlaces();
                            parkingService.GetCapacity();
                            break;
                        case '4':
                            parkingService.GetTransactionsForTheLastMinute();
                            break;
                        case '5':
                            parkingService.ReadFromLog();
                            break;
                        case '6':
                            parkingService.GetVehicles();
                            break;
                        case '7':
                            AddCarToParking();
                            break;
                        case '8':
                            if (parkingService.GetVehicles().Count == 0)
                            {
                                Console.WriteLine("Невозможно удалить . Нету припаркованых автомобилей");
                                break;
                            }
                            Console.Write("Введите Id авто для удаления : ");
                            parkingService.RemoveVehicle(Console.ReadLine());
                            break;
                        case '9':
                            if (parkingService.GetVehicles().Count == 0)
                            {
                                Console.WriteLine("Нету припаркованых автомобилей");
                                break;
                            }
                            Console.Write("Введите Id авто для пополнения : ");
                            string id = Console.ReadLine();
                            Console.Write("\nВведите сумму для пополнения : ");
                            decimal sum = decimal.Parse(Console.ReadLine());
                            
                            parkingService.TopUpVehicle(id, sum);
                            break;
                    }
                    Console.WriteLine("\nДля выхода в меню нажмите на любую клавишу!");
                    Console.ReadKey();
                }
            } while (key.KeyChar != '0');
        }

        private void AddCarToParking()
        {
            Console.WriteLine("Режим добавления авто!\n");
            //if (Settings._capacity == 0)
            //{
            //    Console.WriteLine("Нету свободных мест");
            //    return;
            //}
            try
            {
                Console.Write("Введите номер автомобиля в формате YY-DDDD-YY: ");
                string carNumber = Console.ReadLine();
                Console.Write("Введите сумму пополнения баланса автомобиля: ");
                decimal carBalance = decimal.Parse(Console.ReadLine());
                int carType;
                do
                {
                    Console.Write("Выберите тип автомобиля:\n\t1)Passenger\n\t2)Truck\n\t3)Bus\n\t4)Motorcycle\nВведите нужное число: ");
                    carType = int.Parse(Console.ReadLine());
                } 
                while (carType < 1 || carType > 5);
                Vehicle vehicle = new Vehicle(carNumber, (VehicleType)carType, carBalance);
                parkingService.AddVehicle(vehicle);
            }
            catch (FormatException)
            {
                Console.WriteLine("Ввод некорректный! Баланс и тип авто необходимо вводить числами!");
            }
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
        
        }

    }
}
