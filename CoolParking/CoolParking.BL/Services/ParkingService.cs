﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System.IO;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        //сінглтон екземпляр 
        Parking parking = Parking.getInstance();

        private bool disposed = false;
        //private TimerService withdrawTimer;
        //private TimerService logTimer;
        private ILogService logService;
        Timer payTimer;
        Timer logTimer;
        public ParkingService()
        {
        }
        public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
        {

            //запис в лог
            logTimer = new Timer(_logTimer.Interval = Settings.LogPeriod);
            logTimer.Start();
            logTimer.Elapsed += logTimer_Elapsed;

            //списання  валюти кожних 5 секунд
            payTimer = new Timer(_withdrawTimer.Interval = Settings.PayPeriod);
            payTimer.Start();
            payTimer.Elapsed += WithdrawTimer_Elapsed;
            payTimer.Elapsed += parking.GetTransaction;

            ////создание лога
            logService = _logService;
        }

        //Запись в лог каждую минуту
        private void logTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            logService.Write(PrepareTextForLogging());
        }

        //Списание средств каждых 5 сек
        private void WithdrawTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            parking.WriteOffFunds();
            //Console.ForegroundColor = ConsoleColor.Cyan;
            //Console.WriteLine("***  Списание средств  *** : " + DateTime.Now);
            //Console.ForegroundColor = ConsoleColor.White;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (Settings._capacity == 0)
            {
                throw new InvalidOperationException();
            }

            foreach (Vehicle car in parking.Vehicles)
            {
                if (car.Id == vehicle.Id)
                {
                    throw new ArgumentException();
                }
            }

            if (Settings._tariffs[vehicle.VehicleType] > vehicle.Balance)
                Console.WriteLine("Недостаточно средств для парковки!");
            else
            {
                parking.Vehicles.Add(vehicle);
                Settings._capacity -= 1;  // Зменшуєм кількість місць на парковці
                Console.WriteLine("Количество парковочных мест : " + Settings._capacity);
                Console.WriteLine("Автомобиль был успешно добавлен в паркинг!");
            }

        }
        public void Dispose()
        {
            payTimer.Stop();
            payTimer.Dispose();
            logTimer.Stop();
            logTimer.Dispose();
            parking.ParkingMoneyBalance = 0;
            parking.Vehicles.Clear();
            parking.Vehicles = null;
            parking.Transactions.Clear();
            parking.Transactions = null;
            Settings._capacity = 10;
        }

        public decimal GetBalance()
        {
            Console.WriteLine($"Текущий баланс парковки : {parking.ParkingMoneyBalance}");
            return parking.ParkingMoneyBalance;
        }

        public int GetCapacity()
        {
            Console.WriteLine($"{10 - Settings._capacity} автомобилей припарковано");
            //return Settings._capacity;   // повинно бути так
            return Settings.fullcapacity;
        }

        public int GetFreePlaces()
        {
            Console.WriteLine($"Количество свободных мест : {Settings._capacity} с 10");
            return Settings._capacity;       //ПОВИННО БУТИ ТАК
            //return Settings.fullcapacity;
        }

        //записи транзакций до записи в лог
        public TransactionInfo[] GetLastParkingTransactions()
        {
            TransactionInfo[] transactionInfos = parking.Transactions.ToArray();
            for (int i = 0; i < transactionInfos.Length; i++)
            {
                Console.WriteLine("***\nVehicle ID : " + transactionInfos[i].VehicleId + "  |  $ " + transactionInfos[i].Sum + " | date : " + transactionInfos[i].DateTimeOfTransaction);
            }
            return transactionInfos;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> ReadOnlyVehicles = new ReadOnlyCollection<Vehicle>(parking.Vehicles);
            if (ReadOnlyVehicles.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Парковка пуста ");
                Console.ForegroundColor = ConsoleColor.White;
            }
            foreach (Vehicle vehicle in ReadOnlyVehicles)
            {
                Console.WriteLine($"Id - {vehicle.Id} , тип авто - {vehicle.VehicleType} , баланс - {vehicle.Balance}");
            }
            return ReadOnlyVehicles;
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {

            Vehicle vehicle = parking.FindAutoByNumber(vehicleId);
            if (vehicle != null)
            {
                if (vehicle.Balance < 0)
                {
                    throw new InvalidOperationException();
                    //Console.WriteLine("Нельзя забрать авто с парковки. Оплатите штраф !!!");
                    //return;
                }
                parking.Vehicles.Remove(vehicle);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Авто успешно удалено");
                Console.ForegroundColor = ConsoleColor.White;
                Settings._capacity += 1;  // збільшуєм кількість місць на парковці
                Console.WriteLine("Количество парковочных мест : " + Settings._capacity);
            }
            else
                throw new ArgumentException();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            try
            {
                Vehicle vehicle = parking.FindAutoByNumber(vehicleId);
                if (vehicle != null)
                {
                    Console.WriteLine($"Текущий баланс {vehicle.Id} = {vehicle.Balance}.");
                    if (sum < 0)
                    {
                        Console.WriteLine("Вы не можете уменьшить баланс!");
                        throw new ArgumentException();
                    }
                    else
                    {
                        vehicle.Balance += sum;
                        Console.WriteLine($"Баланс после пополнения = {vehicle.Balance}.");
                    }
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Авто не было найдено!");
                    Console.ForegroundColor = ConsoleColor.White;
                    throw new ArgumentException();
                    //return;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Некорректный ввод! Ожидалось число!");
            }

        }

        public void GetSumForTheLastMinute()
        {
            decimal earnedFundsForTheLastMinute = 0;

            DateTime dateTimeNow = DateTime.Now;
            DateTime dateTimeNowMinuseOneMinute = dateTimeNow.Subtract(new TimeSpan(0, 1, 0));

            foreach (var item in parking.Transactions)
            {
                if (item.DateTimeOfTransaction > dateTimeNowMinuseOneMinute)
                {
                    earnedFundsForTheLastMinute += item.Sum;
                }
            }
            Console.WriteLine("Сума дохода за последнюю минуту равнa : {0}", earnedFundsForTheLastMinute);
            //return earnedFundsForTheLastMinute;
        }

        public void GetTransactionsForTheLastMinute()
        {
            DateTime dateTimeNow = DateTime.Now;
            DateTime dateTimeNowMinuseOneMinute = dateTimeNow.Subtract(new TimeSpan(0, 1, 0));

            foreach (var item in parking.Transactions)
            {
                if (item.DateTimeOfTransaction > dateTimeNowMinuseOneMinute)
                {
                    Console.WriteLine(item.ToString());
                }
            }
        }

        private string PrepareTextForLogging()
        {
            string log = string.Empty;

            foreach (var item in parking.Transactions)
            {
                log += item.ToString();
            }
            return log;
        }

        public bool Duplication(string Id)
        {
            bool flag = false;
            foreach (Vehicle item in parking.Vehicles)
            {
                if (item.Id == Id)
                    flag = true;
            }
            return flag;
        }
    }
}