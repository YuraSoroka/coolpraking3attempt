﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; set; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }
        public string Read()
        {
            string dateFromTransactionLog = string.Empty;

            if (File.Exists(LogPath))
            {
                using (StreamReader streamReader = new StreamReader(LogPath))
                {
                    dateFromTransactionLog = streamReader.ReadToEnd();
                    Console.WriteLine(dateFromTransactionLog);
                }

                return dateFromTransactionLog;
            }
            else
            {
                throw new InvalidOperationException("Файл не найден !");
            }

        }

        public void Write(string logInfo)
        {
            string clearFile = string.Empty;

                using (StreamWriter streamWriter = new StreamWriter(LogPath, true))      // для того, щоб файл перезаписувався, 
                {                                                                        // актуальними даними виставляємо false
                    streamWriter.WriteLine(logInfo);
                }
        }
    }
}